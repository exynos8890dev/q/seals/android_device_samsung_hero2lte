PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/aosip_hero2lte.mk \
	$(LOCAL_DIR)/lineage_hero2lte.mk \
	$(LOCAL_DIR)/cesium_hero2lte.mk \
	$(LOCAL_DIR)/bootleg_hero2lte.mk \
	$(LOCAL_DIR)/potato_hero2lte.mk \
	$(LOCAL_DIR)/aosp_hero2lte.mk \
	$(LOCAL_DIR)/euphoria_hero2lte.mk \
	$(LOCAL_DIR)/havoc_hero2lte.mk \
	$(LOCAL_DIR)/pixys_hero2lte.mk \

COMMON_LUNCH_CHOICES := \
    aosip_hero2lte-userdebug \
    cesium_hero2lte-userdebug \
    lineage_hero2lte-userdebug \
	bootleg_hero2lte-userdebug \
	potato_hero2lte-userdebug \
	aosp_hero2lte-userdebug \
	havoc_hero2lte-userdebug \
    euphoria_hero2lte-userdebug \
    pixys_hero2lte-userdebug \
